(function ($, Drupal, once) {
  Drupal.blikPaymentForm = {
    pollingTimeout: null,
    makeCall(url, settings) {
      settings = settings || {};
      const ajaxSettings = {
        dataType: 'json',
        url: url,
      };
      $.extend(ajaxSettings, settings);
      return $.ajax(ajaxSettings);
    },
    addLoader(confirmMessage, cancelButtonText) {
      const $background = $('<div class="blik-background-overlay"></div>');
      const $loader = $('<div class="blik-background-overlay-loader"></div>');
      const $confirmMessage = $('<div class="blik-background-overlay-message">' + confirmMessage + '</div>');
      const $cancelButton = $('<button class="blik-cancel-button">' + cancelButtonText + '</button>');
      $background.append($loader, $confirmMessage, $cancelButton);
      $('body').append($background);
      $cancelButton.on('click', () => {
        clearTimeout(this.pollingTimeout);
        $background.remove();
      });
    },
    startStatusPolling(url, timeoutDuration, timeoutMessage) {
      const pollingFunction = () => {
        Drupal.blikPaymentForm.makeCall(url, {
          type: 'POST',
          contentType: 'application/json; charset=utf-8',
        }).then((data) => {
          if (data.hasOwnProperty('redirectUrl')) {
            window.location.assign(data.redirectUrl);
          } else {
            // Schedule the next call
            this.pollingTimeout = setTimeout(pollingFunction, 10000);
          }
        }, (data) => {
          this.pollingTimeout = setTimeout(pollingFunction, 10000);
        });
      };

      // Start the polling
      this.pollingTimeout = setTimeout(pollingFunction, 10000);

      // Stop the polling after the specified duration
      setTimeout(() => {
        clearTimeout(this.pollingTimeout);
        const messages = new Drupal.Message();
        const options = {
          type: 'error',
        };
        messages.clear();
        messages.add(timeoutMessage, options);
      }, timeoutDuration);
    },
  };
  Drupal.behaviors.blikPaymentForm = {
    attach(context, settings) {
      for (let index in settings.blikPaymentForm) {
        once('blik-payment-form', '#' + settings.blikPaymentForm[index]['elementId'], context).forEach(input => {
          input.addEventListener('input', function () {
            let value = this.value;
            value = value.replace(/\D/g, '');
            this.value = value;
            if (value.length === 6 && /^\d+$/.test(value)) {
              Drupal.blikPaymentForm.makeCall(settings.blikPaymentForm[index]['onCreateUrl'], {
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                  blik: value,
                }),
              }).then((data) => {
                Drupal.blikPaymentForm.addLoader(settings.blikPaymentForm[index]['confirmMessage'], settings.blikPaymentForm[index]['cancelButtonText']);
                Drupal.blikPaymentForm.startStatusPolling(settings.blikPaymentForm[index]['onCheckUrl'], 120000, settings.blikPaymentForm[index]['timeoutMessage']);
              }, (data) => {
                const messages = new Drupal.Message();
                const options = {
                  type: 'error',
                };
                messages.clear();
                messages.add(data.responseJSON.message, options);
              });
            }
          });
        });
      }
    },
  };
})(jQuery, Drupal, once);
