#### INTRODUCTION

This project integrates imoje payment services with Drupal Commerce, adding two new payment methods:

1. imoje (Off-site redirect)
2. imoje Blik (On-site blik transaction)

**Project Page:** [Drupal.org - commerce_imoje](https://www.drupal.org/project/commerce_imoje)

**Issue Tracking:** [Drupal.org Issues - commerce_imoje](https://www.drupal.org/project/issues/commerce_imoje)

**Learn More About imoje:** [imoje.com](https://www.imoje.com)

This module seamlessly integrates imoje with
Drupal's [Commerce payment and checkout systems](http://drupal.org/project/commerce).

#### REQUIREMENTS

- **Drupal Modules:**
    - Commerce2 ([Drupal.org - Commerce](https://drupal.org/project/commerce))

#### CONFIGURATION

1. Navigate to `"admin/commerce/config/payment-gateways/add"` to add the imoje (Off-site redirect) or imoje Blik Payment
   gateway.
2. Enter the required credentials.
3. Configure notification address in the imoje panel ([imoje.ing.pl](https://imoje.ing.pl/)):

- After logging in, go to the 'Stores' tab.
- Select your shop and click 'Details'.
- In 'Data for integration', edit the 'Notification address' field.
- The notification URL should be your site URL followed by `/payment/notify/{payment_gateway_id}` (found on the payment
  gateway edit page).
- Note: If using both payment methods, you can add either of them.
