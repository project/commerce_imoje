<?php

namespace Drupal\commerce_imoje;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;

interface BlikPaymentFormBuilderInterface {

  /**
   * Builds the Blik payment form.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway.
   *
   * @return array
   *   A renderable array representing the Blik payment form.
   */
  public function build(OrderInterface $order, PaymentGatewayInterface $payment_gateway);

}
