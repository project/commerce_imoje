<?php

namespace Drupal\commerce_imoje\Plugin\Commerce\PaymentGateway;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "imoje_blik",
 *   label = "imoje Blik",
 *   display_label = "Blik",
 *    forms = {
 *     "offsite-payment" =
 *   "Drupal\commerce_imoje\PluginForm\ImojeBlik\ImojeBlikForm",
 *   },
 *   modes = {
 *     "test" = @Translation("Sandbox"),
 *     "live" = @Translation("Production"),
 *   },
 *   payment_type = "imoje_checkout",
 *   requires_billing_information = TRUE,
 * )
 */
class ImojeBlik extends ImojeOffsitePaymentGatewayBase {

}
