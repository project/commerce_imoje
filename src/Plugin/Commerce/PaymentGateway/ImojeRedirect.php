<?php

namespace Drupal\commerce_imoje\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_imoje\ImojeGatewayInterface;
use Drupal\commerce_imoje\IPNHandlerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Url;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "imoje_redirect",
 *   label = "imoje (Off-site redirect)",
 *   display_label = "imoje",
 *    forms = {
 *     "offsite-payment" =
 *   "Drupal\commerce_imoje\PluginForm\ImojeRedirect\ImojePaymentForm",
 *   },
 *   modes = {
 *     "test" = @Translation("Sandbox"),
 *     "live" = @Translation("Production"),
 *   },
 *   payment_type = "imoje_checkout",
 *   requires_billing_information = TRUE,
 * )
 */
class ImojeRedirect extends ImojeOffsitePaymentGatewayBase {

  const FRONT_PROD_URL = 'https://paywall.imoje.pl/payment';

  const FRONT_SANDBOX_URL = 'https://sandbox.paywall.imoje.pl/payment';

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $offsite_payment_methods = [
      'card' => $this->t('Card payment'),
      'pbl' => $this->t('Online transfer payment'),
      'blik' => $this->t('BLIK payment'),
      'imoje_paylater' => $this->t('Imoje pay later payments'),
      'lease' => $this->t('Payment ING Lease Now'),
      'wt' => $this->t('Payment by traditional transfer'),
    ];

    $form['payment_methods'] = [
      '#type' => 'select',
      '#title' => $this->t('Payment methods'),
      '#default_value' => $this->configuration['payment_methods'] ?? [],
      '#description' => 'In imoje it is possible to display only specific payment methods to the Payer.',
      '#options' => $offsite_payment_methods,
      '#multiple' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['payment_methods'] = $values['payment_methods'];
    }
  }

}
