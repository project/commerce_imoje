<?php

namespace Drupal\commerce_imoje\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_imoje\ImojeGatewayInterface;
use Drupal\commerce_imoje\IPNHandlerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Url;

abstract class ImojeOffsitePaymentGatewayBase extends OffsitePaymentGatewayBase implements SupportsRefundsInterface {

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The IPN handler.
   *
   * @var \Drupal\commerce_imoje\IPNHandlerInterface
   */
  protected $ipnHandler;

  /**
   * The imoje gateway.
   *
   * @var \Drupal\commerce_imoje\ImojeGatewayInterface
   */
  protected $imojeGateway;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, IPNHandlerInterface $ipn_handler, ImojeGatewayInterface $imoje_gateway) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->ipnHandler = $ipn_handler;
    $this->imojeGateway = $imoje_gateway;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_imoje.ipn_handler'),
      $container->get('commerce_imoje.payment_gateway')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Credentials'),
    ];

    $form['credentials']['prod_url'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['link-container']],
      'link' => [
        '#title' => $this->t('Configuration panel - imoje'),
        '#type' => 'link',
        '#url' => Url::fromUri('https://imoje.ing.pl/'),
        '#attributes' => ['target' => '_blank'],
        '#options' => ['attributes' => ['target' => '_blank']],
      ],
    ];

    $form['credentials']['sandbox_url'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['link-container']],
      'link' => [
        '#title' => $this->t('Sandbox configuration panel - imoje'),
        '#type' => 'link',
        '#url' => Url::fromUri('https://sandbox.imoje.ing.pl/'),
        '#attributes' => ['target' => '_blank'],
        '#options' => ['attributes' => ['target' => '_blank']],
      ],
    ];

    $form['credentials']['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Identification'),
      '#default_value' => $this->configuration['merchant_id'] ?? '',
      '#description' => $this->t('Merchant identification number'),
      '#required' => TRUE,
    ];

    $form['credentials']['service_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service Identification'),
      '#default_value' => $this->configuration['service_id'] ?? '',
      '#description' => $this->t('Service identification number'),
      '#required' => TRUE,
    ];

    $form['credentials']['service_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service Key'),
      '#default_value' => $this->configuration['service_key'] ?? '',
      '#description' => $this->t('Service key'),
      '#required' => TRUE,
    ];

    $form['credentials']['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Authorization token'),
      '#default_value' => $this->configuration['token'] ?? '',
      '#description' => $this->t('The authorisation token can be found in the settings, in the top right corner of the imoje panel next to your name. Then go to the API Keys tab, select an existing key (default) and go into its Details, where you should copy the existing key.'),
      '#required' => TRUE,
    ];

    $site_url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    $payment_notify_url = $site_url . 'payment/notify/{machine_name}';

    $form['credentials']['notification_url'] = [
      '#type' => 'markup',
      '#markup' => '<div class="messages messages--warning">' . $this->t('For correct configuration of notification dispatch, you must enter the appropriate notification URL in the imoje admin panel. The URL should be @payment_notify_url', ['@payment_notify_url' => $payment_notify_url]) . '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['credentials']['merchant_id'];
      $this->configuration['service_id'] = $values['credentials']['service_id'];
      $this->configuration['service_key'] = $values['credentials']['service_key'];
      $this->configuration['token'] = $values['credentials']['token'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request): void {
    $this->messenger()->addStatus($this->t('Your order finished succesfully'));
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request): void {
    $this->messenger()
      ->addError($this->t('Payment @gateway was unsuccessful, you may resume payment processs anytime.', [
        '@gateway' => $this->getDisplayLabel(),
      ]));
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $service_key = $this->configuration['service_key'];
    $payment_gateway_id = $this->parentEntity->id();
    return $this->ipnHandler->process($request, $service_key, $payment_gateway_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getNotifyUrl(): string {
    return Url::fromRoute('commerce_payment.notify', [
      'commerce_payment_gateway' => $this->parentEntity->id(),
    ], ['absolute' => TRUE])->toString();
  }

  /**
   * Get the return URL.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return string
   */
  public function getReturnUrl(OrderInterface $order): string {
    return Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();
  }

  /**
   * Get the cancel URL.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return string
   */
  public function getCancelUrl(OrderInterface $order): string {
    return Url::fromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();
  }

  /**
   * Creates signature for order data, which is required to send to imoje.
   *
   * @param array $orderData
   * @param string $serviceKey
   * @param string $hashMethod
   *
   * @return string
   */
  public function createSignature(array $orderData, string $serviceKey, string $hashMethod = 'sha256'): string {
    $data = $this->prepareData($orderData);

    return hash($hashMethod, $data . $serviceKey);
  }

  /**
   * Prepare array data for signature calculation.
   *
   * @param array $data
   * @param string $prefix
   *
   * @return string
   */
  protected function prepareData(
    array $data,
    string $prefix = ''
  ): string {
    ksort($data);
    $hashData = [];
    foreach ($data as $key => $value) {
      if ($prefix) {
        $key = $prefix . '[' . $key . ']';
      }
      if (is_array($value)) {
        $hashData[] = $this->prepareData($value, $key);
      }
      else {
        $hashData[] = $key . '=' . $value;
      }
    }

    return implode('&', $hashData);
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL): void {
    try {
      $this->imojeGateway->refundTransaction($payment, $amount);
      $this->messenger()
        ->addMessage($this->t('Refund requested successfully.'));
    }
    catch (BadResponseException $exception) {
      $this->messenger()
        ->addError($this->t('Refund error: @error', ['@error' => $exception->getMessage()]));
      throw new PaymentGatewayException($this->t('Refund error: @error', ['@error' => $exception->getMessage()]));
    }
    catch (\Exception $e) {
      $this->messenger()
        ->addError($this->t('Refund error: @error', ['@error' => $e->getMessage()]));
      throw new PaymentGatewayException($this->t('Refund error: @error', ['@error' => $e->getMessage()]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function canRefundPayment(PaymentInterface $payment): bool {
    if ($payment->getState()->getId() === 'completed') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Gets the currency fraction factor, which is required to get price amount
   * in the lowest nominal.
   *
   * @param string $currency_code
   *
   * @return int
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCurrencyFractionFactor(string $currency_code): int {
    $currency_storage = $this->entityTypeManager->getStorage('commerce_currency');
    $currency = $currency_storage->load($currency_code);
    return pow(10, $currency->getFractionDigits());
  }

  /**
   * Gets the price in the lowest nominal.
   *
   * @param \Drupal\commerce_price\Price $price
   *
   * @return \Drupal\commerce_price\Price
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPriceInLowestNominal(Price $price): Price {
    return $price->multiply((string) $this->getCurrencyFractionFactor($price->getCurrencyCode()));
  }

  /**
   * Gets the price in the standard nominal.
   *
   * @param \Drupal\commerce_price\Price $price
   *
   * @return \Drupal\commerce_price\Price
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPriceInStandardNominal(Price $price): Price {
    return $price->divide((string) $this->getCurrencyFractionFactor($price->getCurrencyCode()));
  }

}
