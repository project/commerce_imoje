<?php

namespace Drupal\commerce_imoje\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the payment type for imoje Checkout.
 *
 * @CommercePaymentType(
 *   id = "imoje_checkout",
 *   label = @Translation("imoje Checkout"),
 *   workflow = "payment_imoje_checkout"
 * )
 */
class ImojeCheckout extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
