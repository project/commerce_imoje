<?php

namespace Drupal\commerce_imoje\Event;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce\EventBase;

/**
 * Defines the imoje payment event.
 */
class ImojePaymentEvent extends EventBase {

  const IMOJE_PAYMENT_RECEIVED = 'commerce_imoje.imoje_payment.received';

  const IMOJE_PAYMENT_UPDATED = 'commerce_imoje.imoje_payment.updated';

  /**
   * The payment.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * The IPN data.
   *
   * @var array
   */
  protected $ipnData;

  /**
   * Constructs a new imoje payment event.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *  The order payment.
   * @param array $ipn_data
   *  The IPN data.
   */
  public function __construct(PaymentInterface $payment, array $ipn_data) {
    $this->payment = $payment;
    $this->ipnData = $ipn_data;
  }

  /**
   * Gets the payment.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The order payment.
   */
  public function getPayment(): PaymentInterface {
    return $this->payment;
  }

  /**
   * Gets the IPN data.
   *
   * @return array
   *   The IPN data.
   */
  public function getIpnData(): array {
    return $this->ipnData;
  }

}
