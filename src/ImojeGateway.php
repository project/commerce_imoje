<?php

namespace Drupal\commerce_imoje;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Square\Http\HttpMethod;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ImojeGateway implements ImojeGatewayInterface {

  const IMOJE_SANDBOX_API_URL = 'https://sandbox.api.imoje.pl/v1';

  const IMOJE_API_URL = 'https://api.imoje.pl/v1';

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new PaymentGatewayBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel.
   * @param \GuzzleHttp\ClientInterface $client
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger, ClientInterface $client, TimeInterface $time) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->client = $client;
    $this->time = $time;
  }

  /**
   * @throws \Exception
   */
  public function getTransactionStatus(PaymentGatewayInterface $commerce_payment_gateway, string $transaction_id): string {
    $config = $commerce_payment_gateway->getPluginConfiguration();
    if (!$transaction_id) {
      throw new \Exception('Transaction id is empty');
    }
    $data = $this->callApi($commerce_payment_gateway, "/merchant/" . $config['merchant_id'] . "/transaction/" . $transaction_id, [], 'GET');
    $transaction = $data['transaction'];
    return $transaction['status'] ?? '';
  }

  /**
   * @throws \Exception|\GuzzleHttp\Exception\GuzzleException
   */
  public function createBlikTransaction(OrderInterface $commerce_order, PaymentGatewayInterface $commerce_payment_gateway, string $blik_code, string $ip_address): array {
    $plugin = $commerce_payment_gateway->getPlugin();
    $config = $plugin->getConfiguration();

    $parameters = [
      'type' => 'sale',
      'serviceId' => $config['service_id'],
      'amount' => (int) $plugin->getPriceInLowestNominal($commerce_order->getTotalPrice())
        ->getNumber(),
      'currency' => $commerce_order->getTotalPrice()->getCurrencyCode(),
      'orderId' => $commerce_order->id(),
      'paymentMethod' => 'blik',
      'paymentMethodCode' => 'blik',
      'successReturnUrl' => $plugin->getReturnUrl($commerce_order),
      'failureReturnUrl' => $plugin->getCancelUrl($commerce_order),
      'clientIp' => $ip_address,
      'blikCode' => $blik_code,
      'validTo' => $this->time->getRequestTime() + 150,
    ];

    if ($commerce_order->getBillingProfile()) {
      $address = $commerce_order->getBillingProfile()->address->first();
      $parameters['customer']['firstName'] = $address->getGivenName();
      $parameters['customer']['lastName'] = $address->getFamilyName();
      $parameters['customer']['email'] = $commerce_order->getEmail();
    }
    return $this->callApi($commerce_payment_gateway, "/merchant/" . $config['merchant_id'] . "/transaction", $parameters);
  }

  /**
   * @throws \Exception|\GuzzleHttp\Exception\GuzzleException
   */
  public function refundTransaction(PaymentInterface $payment, Price $amount = NULL): array {
    $plugin = $payment->getPaymentGateway()->getPlugin();
    $config = $plugin->getConfiguration();

    $parameters = [
      'type' => 'refund',
      'amount' => $amount ? (int) $plugin->getPriceInLowestNominal($amount)
        ->getNumber() : (int) $plugin->getPriceInLowestNominal($payment->getAmount())
        ->getNumber(),
      'serviceId' => $config['service_id'],
    ];

    return $this->callApi($payment->getPaymentGateway(), "/merchant/" . $config['merchant_id'] . "/transaction/" . $payment->getRemoteId() . "/refund", $parameters);
  }

  /**
   * Calls the imoje api with given parameters.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $commerce_payment_gateway
   * @param string $api_url
   * @param array $parameters
   * @param string $method
   *
   * @return array
   * @throws \Exception|\GuzzleHttp\Exception\BadResponseException|\GuzzleHttp\Exception\GuzzleException
   */
  public function callApi(PaymentGatewayInterface $commerce_payment_gateway, string $api_url, array $parameters = [], string $method = 'POST'): array {
    $config = $commerce_payment_gateway->getPluginConfiguration();
    $base_url = $commerce_payment_gateway->getPlugin()
      ->getMode() == 'test' ? self::IMOJE_SANDBOX_API_URL : self::IMOJE_API_URL;
    $api_url = $base_url . $api_url;
    $headers = [
      'Authorization' => 'Bearer ' . $config['token'],
      'Content-Type' => 'application/json',
    ];

    $options = [
      'headers' => $headers,
    ];

    if (!empty($parameters)) {
      $options['body'] = Json::encode($parameters);
    }

    try {
      $response = $this->client->request($method, $api_url, $options);

      $body = $response->getBody()->getContents();
      return Json::decode($body);
    }
    catch (BadResponseException $exception) {
      $this->logger->error($exception->getResponse()
        ->getBody()
        ->getContents(), ['api_url' => $api_url, 'options' => $options]);
      throw $exception;
    }
    catch (\Exception $exception) {
      $this->logger->error($exception->getMessage(), [
        'api_url' => $api_url,
        'options' => $options,
      ]);
      throw $exception;
    }
  }

}
