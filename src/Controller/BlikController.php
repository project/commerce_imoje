<?php

namespace Drupal\commerce_imoje\Controller;

use Drupal\commerce_imoje\ImojeGatewayInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\Exception\BadResponseException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Blik checkout controller.
 */
class BlikController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The imoje gateway.
   *
   * @var \Drupal\commerce_imoje\ImojeGatewayInterface
   */
  protected $imojeGateway;

  /**
   * Constructs a new BlikController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\commerce_imoje\ImojeGatewayInterface $imoje_gateway
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger, ImojeGatewayInterface $imoje_gateway) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->imojeGateway = $imoje_gateway;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.channel.commerce_imoje'),
      $container->get('commerce_imoje.payment_gateway')
    );
  }

  /**
   * Create blik transaction in imoje.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The order.
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $commerce_payment_gateway
   *   The payment gateway.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse A response.
   *   A response.
   */
  public function createTransaction(OrderInterface $commerce_order, PaymentGatewayInterface $commerce_payment_gateway, Request $request): JsonResponse {
    $blik_code = $request->getContent() ? Json::decode($request->getContent())['blik'] : NULL;

    try {
      $data = $this->imojeGateway->createBlikTransaction($commerce_order, $commerce_payment_gateway, $blik_code, $request->getClientIp());

      $transaction = $data['transaction'];
      $transaction_id = $transaction['id'] ?? NULL;

      return new JsonResponse(['id' => $transaction_id]);
    }
    catch (BadResponseException $exception) {
      $this->logger->error($exception->getResponse()->getBody()->getContents());
      $message = $this->t('Transaction failed. Please review your information and try again.');
      return new JsonResponse(['message' => $message], Response::HTTP_BAD_REQUEST);
    }
    catch (\Exception $exception) {
      $this->logger->error($exception->getMessage());
      $message = $this->t('Transaction failed. Please review your information and try again.');
      return new JsonResponse(['message' => $message], Response::HTTP_BAD_REQUEST);
    }
  }

  /**
   * Resolve order status.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The order.
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $commerce_payment_gateway
   *   The payment gateway.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse A response.
   *   A response.
   */
  public function resolveOrderStatus(OrderInterface $commerce_order, PaymentGatewayInterface $commerce_payment_gateway): JsonResponse {
    $plugin = $commerce_payment_gateway->getPlugin();

    if ($commerce_order->isPaid()) {
      return new JsonResponse(['redirectUrl' => $plugin->getReturnUrl($commerce_order)]);
    }
    return new JsonResponse(['status' => 'pending']);
  }

}
