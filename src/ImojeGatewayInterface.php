<?php

namespace Drupal\commerce_imoje;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;

/**
 * Provides a gateway methods for imoje.
 */
interface ImojeGatewayInterface {

  public function callApi(PaymentGatewayInterface $commerce_payment_gateway, string $api_url, array $parameters = [], string $method = 'POST'): array;

  public function createBlikTransaction(OrderInterface $commerce_order, PaymentGatewayInterface $commerce_payment_gateway, string $blik_code, string $ip_address): array;

  public function getTransactionStatus(PaymentGatewayInterface $commerce_payment_gateway, string $transaction_id): string;

  public function refundTransaction(PaymentInterface $payment, Price $amount = NULL): array;

}
