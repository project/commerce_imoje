<?php

namespace Drupal\commerce_imoje;

use Drupal\commerce_imoje\Event\ImojePaymentEvent;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\Component\Serialization\Json;

class IPNHandler implements IPNHandlerInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a new IPNHandler object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger, TimeInterface $time, EventDispatcherInterface $event_dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->time = $time;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function process(Request $request, string $service_key, string $payment_gateway_id): Response {
    $this->validateSignature($request, $service_key);

    $data = Json::decode($request->getContent());
    $transaction = $data['transaction'] ?? NULL;
    if (!$transaction) {
      $this->logger->alert($this->t('Transaction not found'));
      throw new BadRequestHttpException('Transaction not found');
    }

    $tr_id = $transaction['id'];
    $tr_type = $transaction['type'];
    $tr_amount = $transaction['amount'];
    $tr_currency = $transaction['currency'];
    $tr_status = $transaction['status'];
    $tr_order_id = $transaction['orderId'];

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $order = Order::load($tr_order_id);
    $existing_payments = $payment_storage->loadByProperties([
      'order_id' => $tr_order_id,
      'remote_id' => $tr_id,
    ]);
    $existing_payment = reset($existing_payments);

    if (!$existing_payment) {
      /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
      $payment = $payment_storage->create([
        'payment_gateway' => $payment_gateway_id,
        'order_id' => $tr_order_id,
        'remote_id' => $tr_id,
        'remote_state' => $tr_status,
      ]);
      $price = $payment->getPaymentGateway()
        ->getPlugin()
        ->getPriceInStandardNominal(new Price((string) $tr_amount, $tr_currency));
      switch ($tr_type) {
        case 'sale':
          $payment->setAmount($price);
          $payment->setState($tr_status === 'settled' ? 'completed' : $tr_status);
          break;
        case 'refund':
          $paid = $order->getTotalPaid();
          $payment->setRefundedAmount($price);
          $payment->setState($paid > $price ? 'partially_refunded' : 'refunded');
          break;
        default:
          throw new BadRequestHttpException('Unknown transaction type');
      }
      $payment->save();
      $event = new ImojePaymentEvent($payment, $data);
      $this->eventDispatcher->dispatch($event, ImojePaymentEvent::IMOJE_PAYMENT_RECEIVED);
    }
    else {
      $existing_payment->setRemoteState($tr_status);
      $existing_payment->setState($tr_status === 'settled' ? 'completed' : $tr_status);
      $existing_payment->save();
      $event = new ImojePaymentEvent($existing_payment, $data);
      $this->eventDispatcher->dispatch($event, ImojePaymentEvent::IMOJE_PAYMENT_UPDATED);
    }

    return new Response('{"status":"ok"}', Response::HTTP_OK);
  }

  /**
   * Validates the signature.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $service_key
   *   The service key.
   *
   * @return bool
   *   TRUE if the signature is valid, FALSE otherwise.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   */
  protected function validateSignature(Request $request, string $service_key): bool {
    $header_signature = $request->headers->get('X-Imoje-Signature');

    // Replace semicolons with ampersands (&) to make it compatible with parse_str
    $formatted_signature = str_replace(';', '&', $header_signature);
    // Parse the formatted string
    parse_str($formatted_signature, $header_parts);
    $received_signature = $header_parts['signature'] ?? '';
    $received_alg = $header_parts['alg'] ?? '';

    if (empty($received_signature) || empty($received_alg)) {
      $this->logger->alert($this->t('Signature information not found'));
      throw new BadRequestHttpException('Signature information not found');
    }

    $body = $request->getContent();

    $calculated_signature = hash($received_alg, $body . $service_key);
    if ($received_signature !== $calculated_signature) {
      $this->logger->alert($this->t('Signature mismatch'));
      throw new BadRequestHttpException('Signature mismatch');
    }
    return TRUE;
  }

}
