<?php

namespace Drupal\commerce_imoje;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Url;

/**
 * Provides a helper for building the Smart payment buttons.
 */
class BlikPaymentFormBuilder implements BlikPaymentFormBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function build(OrderInterface $order, PaymentGatewayInterface $payment_gateway) {
    $element = [];

    $element_id = Html::getUniqueId('blik-payment-form');

    $create_url = Url::fromRoute('commerce_imoje.blik_transaction.create', [
      'commerce_order' => $order->id(),
      'commerce_payment_gateway' => $payment_gateway->id(),
    ]);

    $check_url = Url::fromRoute('commerce_imoje.blik_transaction.process', [
      'commerce_order' => $order->id(),
      'commerce_payment_gateway' => $payment_gateway->id(),
    ]);

    $element['#attached']['library'][] = 'commerce_imoje/blik_payment_form';
    $element['#attached']['drupalSettings']['blikPaymentForm'][$order->id()] = [
      'elementId' => $element_id,
      'onCreateUrl' => $create_url->toString(),
      'onCheckUrl' => $check_url->toString(),
      'confirmMessage' => t('Please confirm blik payment in your mobile app'),
      'cancelButtonText' => t('Cancel'),
      'timeoutMessage' => t('Transaction was not confirmed in time. Please try again.'),
    ];

    $element += [
      '#type' => 'textfield',
      '#weight' => 100,
      '#maxlength' => 6,
      '#placeholder' => t('Blik'),
      '#attributes' => [
        'class' => ['blik-input'],
        'id' => $element_id,
      ],
    ];

    return $element;
  }

}
