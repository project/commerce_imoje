<?php

namespace Drupal\commerce_imoje\PluginForm\ImojeBlik;

use Drupal\commerce_imoje\BlikPaymentFormBuilderInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Off-site form for Blik Imoje Payment.
 *
 * This is provided as a fallback when no "review" step is present in Checkout.
 */
class ImojeBlikForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The Blik payment form builder.
   *
   * @var \Drupal\commerce_imoje\BlikPaymentFormBuilderInterface
   */
  protected $builder;

  /**
   * Constructs a new PaymentOffsiteForm object.
   *
   * @param \Drupal\commerce_imoje\BlikPaymentFormBuilderInterface $builder
   *   The Blik payment form builder.
   */
  public function __construct(BlikPaymentFormBuilderInterface $builder) {
    $this->builder = $builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_imoje.blik_payment_form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    $form['#attributes']['class'][] = 'blik-payment-form';
    $form['blik_payment_form'] = $this->builder->build($order, $payment->getPaymentGateway());
    $return = Url::fromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
    $form['blik_payment_form']['#suffix'] = Link::fromTextAndUrl($this->t('Go back'), $return)
      ->toString();

    return $form;
  }

}
