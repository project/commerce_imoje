<?php

namespace Drupal\commerce_imoje\PluginForm\ImojeRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Provides the Off-site payment form.
 */
class ImojePaymentForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * TpayPaymentForm constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   */
  public function __construct(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_imoje\Plugin\Commerce\PaymentGateway\ImojeRedirect $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $order = $form_state->getFormObject()->getOrder();

    $plugin_configuration = $payment_gateway_plugin->getConfiguration();
    $service_id = $plugin_configuration['service_id'];
    $merchant_id = $plugin_configuration['merchant_id'];
    $service_key = $plugin_configuration['service_key'];
    $payment_methods = $plugin_configuration['payment_methods'];
    $currency_code = $payment->getAmount()->getCurrencyCode();

    $parameters = [
      'serviceId' => $service_id,
      'merchantId' => $merchant_id,
      'amount' => (int) $payment_gateway_plugin->getPriceInLowestNominal($payment->getAmount())
        ->getNumber(),
      'currency' => $currency_code,
      'orderId' => $order->id(),
      'orderDescription' => t('Order no') . ' ' . $order->id(),
      'customerEmail' => $order->getEmail(),
      'urlSuccess' => $payment_gateway_plugin->getReturnUrl($order),
      'urlReturn' => $payment_gateway_plugin->getCancelUrl($order),
      'urlFailure' => $payment_gateway_plugin->getCancelUrl($order),
    ];

    if ($payment_methods) {
      $parameters['visibleMethod'] = implode(',', $payment_methods);
    }

    if ($order->getBillingProfile()) {
      $address = $order->getBillingProfile()->address->first();
      $parameters['customerFirstName'] = $address->getGivenName();
      $parameters['customerLastName'] = $address->getFamilyName();
    }
    $hash_method = 'sha256';
    $signature = $payment_gateway_plugin->createSignature($parameters, $service_key, $hash_method);
    $parameters['signature'] = $signature . ';' . $hash_method;

    if ($payment_gateway_plugin->getMode() == 'test') {
      $redirect_url = $payment_gateway_plugin::FRONT_SANDBOX_URL;
    }
    else {
      $redirect_url = $payment_gateway_plugin::FRONT_PROD_URL;
    }

    return $this->buildRedirectForm($form, $form_state, $redirect_url, $parameters);
  }

}
