<?php

namespace Drupal\commerce_imoje;

use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a handler for IPN requests from imoje.
 */
interface IPNHandlerInterface {

  /**
   * Processes an incoming IPN request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return mixed
   *   The request data array.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function process(Request $request, string $service_key, string $payment_gateway_id);

}
